#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <getopt.h>
#include <gcrypt.h>

#define DEFAULT_ITER 10

// Check if file exists, if not create it
// Return file pointer
FILE* get_file(char* file_path){
    FILE* fp = fopen(file_path, "r");

    if(fp){
        printf("Found file: %s\n", file_path);
    } else {
        printf("No such file: %s, attempting to create.\n", file_path);

        fp = fopen(file_path, "w");

        if(fp){
            printf("Created file %s\n", file_path);
        } else {
            printf("Failed to create file: %s\n", file_path);
            exit(1);
        }
    }

    return fp;
}

long get_file_size(FILE* fp){
    long fsize;

    fseek(fp, 0, SEEK_END);
    fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    return fsize;
}

// Open a file and return a file pointer
FILE* open_file(char* file_path){
    FILE* fp = fopen(file_path, "r");

    return fp;
}

// Get file size, read with fread, return fp?
char* read_file(FILE* fp, long fsize){
    char* buff = (char *)malloc(sizeof(char)*fsize);
    
    fread(buff, sizeof(char), fsize, fp);

    return buff;
}

char* hash_data(char* s){
    unsigned char *x;
    unsigned i;
    unsigned int l = gcry_md_get_algo_dlen(GCRY_MD_SHA256); /* get digest length (used later to print the result) */

    gcry_md_hd_t h;
    gcry_md_open(&h, GCRY_MD_SHA256, GCRY_MD_FLAG_SECURE); /* initialise the hash context */
    gcry_md_write(h, s, strlen(s)); /* hash some text */
    x = gcry_md_read(h, GCRY_MD_SHA256); /* get the result */

    return x;
}

int main(int argc, char* argv[]){
    char* file_path = NULL;
    char* temp;
    FILE* fp;
    long fsize;

    clock_t begin, end;
    double timer;
    double timer_ms;

    int opt;
    int count = DEFAULT_ITER;

    unsigned char* hash;


    while( (opt = getopt(argc, argv, "f:n:?")) != -1){
        switch(opt){
            case 'f':
                file_path = (char *)malloc(strlen(optarg)*sizeof(char));
                strcpy(file_path, optarg);
                break;
            case 'n':
                count = atoi(optarg);
                break;
            case '?':
                printf("Usage: %s -f <file_path> [-n]", argv[0]);
                break;

        }
    }

    if(!file_path){
        printf("Missing file argument, aborting\n");
        exit(1);
    }
    fp = get_file(file_path);
    fclose(fp);         // Close so can be opened and measured


    for(int i = count; i > 0; i--){
        printf("---\nStart iteration: %d\n\n", count - i);

        // Time opening a file
        begin = clock();    
        fp = open_file(file_path);
        end = clock();    
        timer = ((double)(end - begin) / CLOCKS_PER_SEC); // Seconds
        timer_ms = timer * 1000; // Convert to milliseconds

        printf("fopen:\n%fms\n\n", timer_ms);

        // Time reading a file
        fsize = get_file_size(fp);
        begin = clock();    
        temp = read_file(fp, fsize);
        end = clock();

        timer = ((double)(end - begin) / CLOCKS_PER_SEC);
        timer_ms = timer * 1000;

        printf("fread:\nread %ld bytes in %fms\n", fsize, timer_ms);
        printf("read speed: %fMB/s\n\n", ((fsize / (1024 * 1024))/timer));

        // Hash
        begin = clock();   
        hash = hash_data(temp);
        end = clock();
        printf("----breakpoint---");;

        timer = ((double)(end - begin) / CLOCKS_PER_SEC); 
        timer_ms = timer * 1000;

        printf("sha256:\nhashed %ld bytes in %fms\n", fsize, timer_ms);
        printf("Hash speed: %fMB/s\n\n", ((fsize / (1024 * 1024))/timer));

        printf("End iteration: %d\n---\n\n", count - i);

        if(temp) free(temp);
        if(fp) fclose(fp);
    }

    printf("\n\n-- Finished loop --\n\n");

   return 0;
}
