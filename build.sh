#!/bin/bash

input="${1}"

SRC="main.c"
BIN="io_test"

rc="0"

echo "Compiling ${SRC} into ${BIN}"
# gcc -o crypt main.c -L/usr/lib/x86_64-linux-gnu -lgcrypt
gcc "${SRC}" -o "${BIN}" -L/usr/lib/x86_64-linux-gnu -lgcrypt

rc="${?}"

if [[ ${rc} -eq 0 ]]; then
    echo "Running ${BIN}"
    ./${BIN} -f ${input}
else
    echo "Failed to compile ${SRC} into ${BIN}"
fi

